﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OsuSync
{
    //https://stackoverflow.com/questions/995195/how-can-i-make-a-net-windows-forms-application-that-only-runs-in-the-system-tra
    public class OsuSyncContextMenu : ApplicationContext
    {
        private NotifyIcon trayIcon;

        public OsuSyncContextMenu()
        {
            // Initialize Tray Icon
            if (Program.Mode == ExecutionMode.Simple)
            {
                trayIcon = new NotifyIcon()
                {
                    Icon = Properties.Resources.MainIcon,
                    ContextMenu = new ContextMenu(new MenuItem[] {
                    new MenuItem("Sync", Sync),
                    new MenuItem("Exit", Exit)
                }),
                    Visible = true
                };
            } else
            {
                trayIcon = new NotifyIcon()
                {
                    Icon = Properties.Resources.MainIcon,
                    ContextMenu = new ContextMenu(new MenuItem[] {
                    new MenuItem("Settings...", OpenSettings),
                    new MenuItem("Sync", Sync),
                    new MenuItem("Exit", Exit)
                }),
                    Visible = true
                };
            }

            Program.CheckDirs();
        }

        void OpenSettings(object sender, EventArgs e)
        {
            SettingsWindow settingswindow = new SettingsWindow();
            settingswindow.Visible = false;
            settingswindow.ShowDialog();
            settingswindow.Visible = true;

        }

        void Sync(object sender, EventArgs e)
        {
            Program.Sync();
        }

        void Exit(object sender, EventArgs e)
        {
            trayIcon.Visible = false;

            Application.Exit();
        }

        //void Scan(object sender, EventArgs e)
        //{
        //    // Hide tray icon, otherwise it will remain shown until user mouses over it
        //    //trayIcon.Visible = false;

        //    GameInfoScanner.Scan();
        //    //Application.Exit();
        //}

        
    }
}
