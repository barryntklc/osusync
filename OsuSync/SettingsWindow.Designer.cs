﻿namespace OsuSync
{
    partial class SettingsWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SettingsTabControl = new System.Windows.Forms.TabControl();
            this.GeneralTab = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.SkinsDirOpenButton = new System.Windows.Forms.Button();
            this.SkinsDirTextBox = new System.Windows.Forms.TextBox();
            this.SkinsDirPickButton = new System.Windows.Forms.Button();
            this.SkinsSyncComboBox = new System.Windows.Forms.ComboBox();
            this.SongsGroupBox = new System.Windows.Forms.GroupBox();
            this.SongsDirOpenButton = new System.Windows.Forms.Button();
            this.SongsDirTextBox = new System.Windows.Forms.TextBox();
            this.SongsDirPickButton = new System.Windows.Forms.Button();
            this.SongsSyncComboBox = new System.Windows.Forms.ComboBox();
            this.SaveButton = new System.Windows.Forms.Button();
            this.CancelButton = new System.Windows.Forms.Button();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.SettingsTabControl.SuspendLayout();
            this.GeneralTab.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SongsGroupBox.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.SuspendLayout();
            // 
            // SettingsTabControl
            // 
            this.SettingsTabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SettingsTabControl.Controls.Add(this.GeneralTab);
            this.SettingsTabControl.Controls.Add(this.tabPage1);
            this.SettingsTabControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SettingsTabControl.Location = new System.Drawing.Point(13, 13);
            this.SettingsTabControl.Name = "SettingsTabControl";
            this.SettingsTabControl.SelectedIndex = 0;
            this.SettingsTabControl.Size = new System.Drawing.Size(563, 242);
            this.SettingsTabControl.TabIndex = 0;
            // 
            // GeneralTab
            // 
            this.GeneralTab.Controls.Add(this.groupBox1);
            this.GeneralTab.Controls.Add(this.SongsGroupBox);
            this.GeneralTab.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GeneralTab.Location = new System.Drawing.Point(4, 29);
            this.GeneralTab.Name = "GeneralTab";
            this.GeneralTab.Padding = new System.Windows.Forms.Padding(3);
            this.GeneralTab.Size = new System.Drawing.Size(555, 209);
            this.GeneralTab.TabIndex = 0;
            this.GeneralTab.Text = "General";
            this.GeneralTab.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.SkinsDirOpenButton);
            this.groupBox1.Controls.Add(this.SkinsDirTextBox);
            this.groupBox1.Controls.Add(this.SkinsDirPickButton);
            this.groupBox1.Controls.Add(this.SkinsSyncComboBox);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(6, 106);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(542, 94);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Skins";
            // 
            // SkinsDirOpenButton
            // 
            this.SkinsDirOpenButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.SkinsDirOpenButton.Location = new System.Drawing.Point(367, 56);
            this.SkinsDirOpenButton.Name = "SkinsDirOpenButton";
            this.SkinsDirOpenButton.Size = new System.Drawing.Size(169, 21);
            this.SkinsDirOpenButton.TabIndex = 2;
            this.SkinsDirOpenButton.Text = "Open Skins Folder...";
            this.SkinsDirOpenButton.UseVisualStyleBackColor = true;
            // 
            // SkinsDirTextBox
            // 
            this.SkinsDirTextBox.Location = new System.Drawing.Point(107, 26);
            this.SkinsDirTextBox.Name = "SkinsDirTextBox";
            this.SkinsDirTextBox.ReadOnly = true;
            this.SkinsDirTextBox.Size = new System.Drawing.Size(429, 24);
            this.SkinsDirTextBox.TabIndex = 1;
            // 
            // SkinsDirPickButton
            // 
            this.SkinsDirPickButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SkinsDirPickButton.Location = new System.Drawing.Point(7, 26);
            this.SkinsDirPickButton.Name = "SkinsDirPickButton";
            this.SkinsDirPickButton.Size = new System.Drawing.Size(93, 24);
            this.SkinsDirPickButton.TabIndex = 0;
            this.SkinsDirPickButton.Text = "...";
            this.SkinsDirPickButton.UseVisualStyleBackColor = true;
            // 
            // SkinsSyncComboBox
            // 
            this.SkinsSyncComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SkinsSyncComboBox.FormattingEnabled = true;
            this.SkinsSyncComboBox.Items.AddRange(new object[] {
            "Sync all skins",
            "Don\'t sync skins",
            "Only pull skins",
            "Only push skins"});
            this.SkinsSyncComboBox.Location = new System.Drawing.Point(7, 56);
            this.SkinsSyncComboBox.Name = "SkinsSyncComboBox";
            this.SkinsSyncComboBox.Size = new System.Drawing.Size(354, 21);
            this.SkinsSyncComboBox.TabIndex = 0;
            // 
            // SongsGroupBox
            // 
            this.SongsGroupBox.Controls.Add(this.SongsDirOpenButton);
            this.SongsGroupBox.Controls.Add(this.SongsDirTextBox);
            this.SongsGroupBox.Controls.Add(this.SongsDirPickButton);
            this.SongsGroupBox.Controls.Add(this.SongsSyncComboBox);
            this.SongsGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SongsGroupBox.Location = new System.Drawing.Point(6, 6);
            this.SongsGroupBox.Name = "SongsGroupBox";
            this.SongsGroupBox.Size = new System.Drawing.Size(542, 94);
            this.SongsGroupBox.TabIndex = 5;
            this.SongsGroupBox.TabStop = false;
            this.SongsGroupBox.Text = "Songs";
            // 
            // SongsDirOpenButton
            // 
            this.SongsDirOpenButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SongsDirOpenButton.Location = new System.Drawing.Point(367, 56);
            this.SongsDirOpenButton.Name = "SongsDirOpenButton";
            this.SongsDirOpenButton.Size = new System.Drawing.Size(169, 21);
            this.SongsDirOpenButton.TabIndex = 2;
            this.SongsDirOpenButton.Text = "Open Songs Folder...";
            this.SongsDirOpenButton.UseVisualStyleBackColor = true;
            // 
            // SongsDirTextBox
            // 
            this.SongsDirTextBox.Location = new System.Drawing.Point(106, 26);
            this.SongsDirTextBox.Name = "SongsDirTextBox";
            this.SongsDirTextBox.ReadOnly = true;
            this.SongsDirTextBox.Size = new System.Drawing.Size(429, 24);
            this.SongsDirTextBox.TabIndex = 1;
            // 
            // SongsDirPickButton
            // 
            this.SongsDirPickButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SongsDirPickButton.Location = new System.Drawing.Point(7, 26);
            this.SongsDirPickButton.Name = "SongsDirPickButton";
            this.SongsDirPickButton.Size = new System.Drawing.Size(93, 24);
            this.SongsDirPickButton.TabIndex = 0;
            this.SongsDirPickButton.Text = "...";
            this.SongsDirPickButton.UseVisualStyleBackColor = true;
            // 
            // SongsSyncComboBox
            // 
            this.SongsSyncComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SongsSyncComboBox.FormattingEnabled = true;
            this.SongsSyncComboBox.Items.AddRange(new object[] {
            "Sync all songs",
            "Don\'t sync songs",
            "Only pull songs",
            "Only push songs"});
            this.SongsSyncComboBox.Location = new System.Drawing.Point(7, 56);
            this.SongsSyncComboBox.Name = "SongsSyncComboBox";
            this.SongsSyncComboBox.Size = new System.Drawing.Size(354, 21);
            this.SongsSyncComboBox.TabIndex = 0;
            // 
            // SaveButton
            // 
            this.SaveButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.SaveButton.Location = new System.Drawing.Point(382, 261);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(94, 23);
            this.SaveButton.TabIndex = 1;
            this.SaveButton.Text = "Save";
            this.SaveButton.UseVisualStyleBackColor = true;
            // 
            // CancelButton
            // 
            this.CancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.CancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CancelButton.Location = new System.Drawing.Point(482, 261);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(94, 23);
            this.CancelButton.TabIndex = 2;
            this.CancelButton.Text = "Cancel";
            this.CancelButton.UseVisualStyleBackColor = true;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.button2);
            this.tabPage1.Controls.Add(this.textBox1);
            this.tabPage1.Controls.Add(this.button1);
            this.tabPage1.Location = new System.Drawing.Point(4, 29);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(555, 209);
            this.tabPage1.TabIndex = 1;
            this.tabPage1.Text = "Share";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(112, 14);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(429, 26);
            this.textBox1.TabIndex = 3;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(13, 14);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(93, 26);
            this.button1.TabIndex = 2;
            this.button1.Text = "...";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(372, 46);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(169, 21);
            this.button2.TabIndex = 4;
            this.button2.Text = "Open Folder...";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // SettingsWindow
            // 
            this.AcceptButton = this.SaveButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.CancelButton;
            this.CausesValidation = false;
            this.ClientSize = new System.Drawing.Size(588, 296);
            this.Controls.Add(this.CancelButton);
            this.Controls.Add(this.SaveButton);
            this.Controls.Add(this.SettingsTabControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SettingsWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "OsuSync - Settings";
            this.SettingsTabControl.ResumeLayout(false);
            this.GeneralTab.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.SongsGroupBox.ResumeLayout(false);
            this.SongsGroupBox.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl SettingsTabControl;
        private System.Windows.Forms.TabPage GeneralTab;
        private System.Windows.Forms.Button SaveButton;
        private System.Windows.Forms.Button CancelButton;
        private System.Windows.Forms.ComboBox SongsSyncComboBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button SkinsDirOpenButton;
        private System.Windows.Forms.TextBox SkinsDirTextBox;
        private System.Windows.Forms.Button SkinsDirPickButton;
        private System.Windows.Forms.ComboBox SkinsSyncComboBox;
        private System.Windows.Forms.GroupBox SongsGroupBox;
        private System.Windows.Forms.Button SongsDirOpenButton;
        private System.Windows.Forms.TextBox SongsDirTextBox;
        private System.Windows.Forms.Button SongsDirPickButton;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button1;
    }
}