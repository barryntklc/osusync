﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Permissions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OsuSync
{
    public static class ExecutionMode
    {
        public static int Simple = 1;
        public static int Full = 0;
    }
    static class Program
    {
        public static String DefaultSettingsDir = Environment.ExpandEnvironmentVariables(@"%userprofile%\Documents\ChunSoft\OsuSync");
        public static String DefaultSettingsFilePath = Path.Combine(DefaultSettingsDir, "settings.json");

        public static string OsuRemotePath = "";
        public static String OsuHomePath = "";
        //public static String SkinsFolder = Path.Combine(OsuHomePath, "Skins");
        //public static String SongsFolder = Path.Combine(OsuHomePath, "Songs");

        public static Settings settings = new Settings(DefaultSettingsFilePath);

        public static int Mode = ExecutionMode.Simple; 

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Console.WriteLine(Directory.Exists(@"O:\osu!"));
            Console.WriteLine(Directory.Exists(@"\\fs\Barryn Chun\osu!"));
            Console.WriteLine(Directory.Exists(@"\\localghost-serv\Barryn Chun\osu!"));

            //Application.EnableVisualStyles();
            //Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new MainWindow());

            //Watch(@"D:\Osu\Songs");
            //CheckDirs();

            //OsuSyncContextMenu menu = new OsuSyncContextMenu();
            //menu.MainForm.Visible = true;
            Application.Run(new OsuSyncContextMenu());

            settings.Write(null);
        }

        public static Boolean IsOsuFolder(String DirPath)
        {
            bool DefaultOsuExists = (Directory.Exists(DirPath));
            bool DefaultOsuSongsExists = (Directory.Exists(Path.Combine(DirPath, "Songs")));
            bool DefaultOsuSkinsExists = (Directory.Exists(Path.Combine(DirPath, "Skins")));

            return DefaultOsuExists && DefaultOsuSongsExists && DefaultOsuSkinsExists;
        }

        public static bool CheckDirs()
        {
            if (settings.LocalDir.Equals("") || (!IsOsuFolder(settings.LocalDir)))
            {
                String title = "OsuSync Setup - Choose Local Folder";
                String description = ("Please choose your Osu! folder.");

                SetPathWindow localpathwindow = new SetPathWindow();
                localpathwindow.Text = title;
                localpathwindow.DescriptionLabel.Text = description;
                localpathwindow.DirTextBox.Text = settings.LocalDir;
                localpathwindow.ShowDialog();

                if (localpathwindow.DialogResult == DialogResult.OK)
                {
                    settings.LocalDir = (localpathwindow.DirTextBox.Text);
                }

                localpathwindow.Close();
            }
            //if (settings.RemoteDir.Equals("") || (!IsOsuFolder(settings.RemoteDir)))
            if (!Directory.Exists(settings.RemoteDir))
            {
                String title = "OsuSync Setup - Choose Remote Folder";
                String description = ("Please choose the remote Osu! folder.");

                SetPathWindow remotepathwindow = new SetPathWindow();
                remotepathwindow.Text = title;
                remotepathwindow.DescriptionLabel.Text = description;
                remotepathwindow.DirTextBox.Text = settings.RemoteDir;
                remotepathwindow.ShowDialog();

                if (remotepathwindow.DialogResult == DialogResult.OK)
                {
                    settings.RemoteDir = (remotepathwindow.DirTextBox.Text);
                }

                remotepathwindow.Close();
            }

            settings.Write(null);

            //return ((settings.LocalDir.Equals("") || (!IsOsuFolder(settings.LocalDir))) && (settings.RemoteDir.Equals("") || (!IsOsuFolder(settings.RemoteDir))));
            return ((!settings.LocalDir.Equals("") || (IsOsuFolder(settings.LocalDir))) && (Directory.Exists(settings.RemoteDir)));
        }

        public static void Sync()
        {
            if (CheckDirs())
            {
                String RemoteSongsDir = Path.Combine(settings.RemoteDir, "Songs");
                String RemoteSkinsDir = Path.Combine(settings.RemoteDir, "Skins");
                if (!Directory.Exists(RemoteSongsDir))
                {
                    Directory.CreateDirectory(RemoteSongsDir);
                }
                if (!Directory.Exists(RemoteSkinsDir))
                {
                    Directory.CreateDirectory(RemoteSkinsDir);
                }

                String LocalSongsDir = Path.Combine(settings.LocalDir, "Songs");
                String LocalSkinsDir = Path.Combine(settings.LocalDir, "Skins");

                //for songs
                if (settings.SongSyncSetting == SyncMode.SyncAll)
                {
                    System.Diagnostics.Process.Start("cmd.exe", ("/C robocopy \"" + LocalSongsDir + "\" \"" + RemoteSongsDir + "\" /E /R:1 /W:0 & robocopy \"" + RemoteSongsDir + "\" \"" + LocalSongsDir + "\" /E /R:1 /W:0"));
                }
                else if (settings.SongSyncSetting == SyncMode.SyncNone)
                {

                }
                else if (settings.SongSyncSetting == SyncMode.PullOnly)
                {

                }
                else if (settings.SongSyncSetting == SyncMode.PushOnly)
                {

                }

                //if push_music
                //push music
                //if pull_music
                //pull music

                //for skins
                if (settings.SkinSyncSetting == SyncMode.SyncAll)
                {
                    System.Diagnostics.Process.Start("cmd.exe", ("/C robocopy \"" + LocalSkinsDir + "\" \"" + RemoteSkinsDir + "\" /E /R:1 /W:0 & robocopy \"" + RemoteSkinsDir + "\" \"" + LocalSkinsDir + "\" /E /R:1 /W:0"));

                }
                else if (settings.SkinSyncSetting == SyncMode.SyncNone)
                {

                }
                else if (settings.SkinSyncSetting == SyncMode.PullOnly)
                {

                }
                else if (settings.SkinSyncSetting == SyncMode.PushOnly)
                {

                }

                //if push_skins
                //push skins
                //if pull_skins
                //pull skins
            } else
            {
                String title = "Invalid Path";
                String description = ("Could not sync: Invalid Path\n" +
                    "Please check if your local and remote folders exist.");

                MessageBox.Show(description, title, MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
        private static void Watch(String WatchPath)
        {
            string[] args = Environment.GetCommandLineArgs();

            

            // Create a new FileSystemWatcher and set its properties.
            using (FileSystemWatcher watcher = new FileSystemWatcher())
            {
                watcher.Path = WatchPath;

                // Watch for changes in LastAccess and LastWrite times, and
                // the renaming of files or directories.
                watcher.NotifyFilter = NotifyFilters.LastAccess
                                     | NotifyFilters.LastWrite
                                     | NotifyFilters.FileName
                                     | NotifyFilters.DirectoryName;

                // Only watch text files.
                //watcher.Filter = "*.txt";

                // Add event handlers.
                watcher.Changed += OnChanged;
                watcher.Created += OnChanged;
                watcher.Deleted += OnChanged;
                watcher.Renamed += OnRenamed;

                // Begin watching.
                watcher.EnableRaisingEvents = true;

                // Wait for the user to quit the program.
                Console.WriteLine("Press 'q' to quit the sample.");
                while (Console.Read() != 'q') ;
            }
        }

        // Define the event handlers.
        private static void OnChanged(object source, FileSystemEventArgs e) =>
            // Specify what is done when a file is changed, created, or deleted.
            Console.WriteLine($"File: {e.FullPath} {e.ChangeType}");

            //update new songs counter
            //if new songs greater than 10 and notifications are true

        private static void OnRenamed(object source, RenamedEventArgs e) =>
            // Specify what is done when a file is renamed.
            Console.WriteLine($"File: {e.OldFullPath} renamed to {e.FullPath}");
    }
}
