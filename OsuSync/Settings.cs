﻿using System;
using System.IO;
using Newtonsoft.Json;

namespace OsuSync
{
    public static class SyncMode
    {
        public static int SyncAll = 0;
        public static int SyncNone = 1;
        public static int PullOnly = 2;
        public static int PushOnly = 3;
    }
    class Settings
    {
        [JsonIgnore]
        public static String DefaultOsuDir = Environment.ExpandEnvironmentVariables(@"%userprofile%\AppData\Local\osu!");
        [JsonIgnore]
        public String DefaultSettingsFilePath;

        //public static String DefaultDir
        public String LocalDir = "";
        public String RemoteDir = "";
        
        public int SongSyncSetting = SyncMode.SyncAll;
        public int SkinSyncSetting = SyncMode.SyncAll;

        //[JsonIgnore]
        //public String SongsDir = Path.Combine(DefaultOsuDir, "Songs");
        //[JsonIgnore]
        //public String SkinsDir = Path.Combine(DefaultOsuDir, "Skins");

        public Settings()
        {
        }

        public Settings(String Path)
        {
            this.DefaultSettingsFilePath = Path;
            Init();
        }

        public void Init()
        {
            String SettingsParentFolder = Directory.GetParent(DefaultSettingsFilePath).FullName;

            if (!Directory.Exists(SettingsParentFolder))
            {
                Directory.CreateDirectory(SettingsParentFolder);
            }

            //if settings file exists
            if (File.Exists(DefaultSettingsFilePath))
            {
                Read(DefaultSettingsFilePath);
            } else
            {
                if (Program.IsOsuFolder(DefaultOsuDir))
                {
                    this.LocalDir = DefaultOsuDir;
                }

                Write(DefaultSettingsFilePath);
            }

            //consider moving to different directory
            //if (LocalDir.Equals("") || (!IsOsuFolder(LocalDir)))
            //{
            //    String title = "OsuSync Setup - Choose Local Folder";
            //    String description = ("Please choose your Osu! folder.");

            //    SetPathWindow pathwindow = new SetPathWindow();
            //    pathwindow.Text = title;
            //    pathwindow.DescriptionLabel.Text = description;
            //    pathwindow.DirTextBox.Text = this.LocalDir;
            //    pathwindow.Visible = false;
            //    pathwindow.ShowDialog();
            //    pathwindow.Visible = true;

            //    this.LocalDir = (pathwindow.DirTextBox.Text);
            //}
            //if (RemoteDir.Equals("") || (!IsOsuFolder(RemoteDir)))
            //{
            //    String title = "OsuSync Setup - Choose Remote Folder";
            //    String description = ("Please choose the remote Osu! folder.");

            //    SetPathWindow pathwindow = new SetPathWindow();
            //    pathwindow.Text = title;
            //    pathwindow.DescriptionLabel.Text = description;
            //    pathwindow.DirTextBox.Text = this.RemoteDir;
            //    pathwindow.Visible = false;
            //    pathwindow.ShowDialog();
            //    pathwindow.Visible = true;

            //    this.RemoteDir = (pathwindow.DirTextBox.Text);
            //}
        }

        public void Read(String SettingsFilePath)
        {
            Settings obj = JsonConvert.DeserializeObject<Settings>(File.ReadAllText(DefaultSettingsFilePath));

            this.LocalDir = obj.LocalDir;
            this.RemoteDir = obj.RemoteDir;
            this.SongSyncSetting = obj.SongSyncSetting;
            this.SkinSyncSetting = obj.SkinSyncSetting;

            //this.SongsDir = obj.SongsDir;
            //this.SkinsDir = obj.SkinsDir;
        }

        public void Write(String SettingsFilePath)
        {
            if (SettingsFilePath == null)
            {
                String ParentFolder = Directory.GetParent(DefaultSettingsFilePath).FullName;
                if (!Directory.Exists(ParentFolder))
                {
                    Directory.CreateDirectory(ParentFolder);
                }

                File.WriteAllText(this.DefaultSettingsFilePath, (JsonConvert.SerializeObject(this, Formatting.Indented)));
            } else
            {
                String ParentFolder = Directory.GetParent(SettingsFilePath).FullName;
                if (!Directory.Exists(ParentFolder))
                {
                    Directory.CreateDirectory(ParentFolder);
                }

                File.WriteAllText(SettingsFilePath, (JsonConvert.SerializeObject(this, Formatting.Indented)));
            }
        }

        override public String ToString()
        {
            string buffer = "";
            buffer += LocalDir;
            buffer += RemoteDir;
            buffer += SongSyncSetting;
            buffer += SkinSyncSetting;
            return "";
        }
    }
}
