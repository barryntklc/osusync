﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OsuSync
{
    public partial class SetPathWindow : Form
    {
        public SetPathWindow()
        {
            InitializeComponent();
        }

        private void DirPickButton_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderpicker = new FolderBrowserDialog();

            DialogResult result = folderpicker.ShowDialog();

            if (result == DialogResult.OK && !String.IsNullOrWhiteSpace(folderpicker.SelectedPath))
            {
                this.DirTextBox.Text = folderpicker.SelectedPath;
            }

        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
